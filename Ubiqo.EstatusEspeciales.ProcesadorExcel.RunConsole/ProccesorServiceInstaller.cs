﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace Ubiqo.EstatusEspeciales.ProcesadorExcel.RunConsole
{
    [RunInstaller(true)]
    public class ProccesorServiceInstaller : Installer
    {
        public ProccesorServiceInstaller()
        {
            ServiceProcessInstaller serviceProcessInstaller = new ServiceProcessInstaller();
            ServiceInstaller serviceInstaller = new ServiceInstaller();

            // Setup the Service Account type per your requirement
            serviceProcessInstaller.Account = ServiceAccount.LocalSystem;
            serviceProcessInstaller.Username = null;
            serviceProcessInstaller.Password = null;

            serviceInstaller.ServiceName = "ServicioProcesadorEstatusEspeciales";
            serviceInstaller.DisplayName = "MotorProcesadorEstatusEspeciales";
            serviceInstaller.StartType = ServiceStartMode.Automatic;
            serviceInstaller.Description = "Procesa y carga Excel de Estatus Especiales";

            this.Installers.Add(serviceProcessInstaller);
            this.Installers.Add(serviceInstaller);
        }
    }
}
