﻿using Ninject;
using System;
using System.ServiceProcess;
using Ubiqo.Cassandra;
using Ubiqo.EstatusEspeciales.ProcesadorExcel.Interfaces;

namespace Ubiqo.EstatusEspeciales.ProcesadorExcel.RunConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            if ((!Environment.UserInteractive))
            {
                Program.RunAsAService();
            }
            else
            {
                if (args != null && args.Length > 0)
                {
                    if (args[0].Equals("-i", StringComparison.OrdinalIgnoreCase))
                    {
                        SelfInstaller.InstallMe();
                    }
                    else
                    {
                        if (args[0].Equals("-u", StringComparison.OrdinalIgnoreCase))
                        {
                            SelfInstaller.UninstallMe();
                        }
                        else
                        {
                            Console.WriteLine("Invalid argument!");
                        }
                    }
                }
                else
                {
                    Program.RunAsAConsole();
                }
            }
        }

        static void RunAsAConsole()
        {
            var kernel = new StandardKernel();
            kernel.Load(new EstatusEspecialesProcesadorModule());
            var lectorExcel = kernel.Get<ILector>();
            var conexionCassandra = kernel.Get<UbiqoCassandraBof>();

            var procesador = new Procesador(lectorExcel , conexionCassandra);
            procesador.Ejecutar();
        }

        static void RunAsAService()
        {
            ServiceBase[] servicesToRun = new ServiceBase[]
           {
                new ProccesorService()
           };
            ServiceBase.Run(servicesToRun);
        }
    }
}
