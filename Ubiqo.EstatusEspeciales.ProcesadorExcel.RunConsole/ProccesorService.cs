﻿using Ninject;
using Softelligence.Log;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Ubiqo.Cassandra;
using Ubiqo.EstatusEspeciales.ProcesadorExcel.Interfaces;

namespace Ubiqo.EstatusEspeciales.ProcesadorExcel.RunConsole
{
    partial class ProccesorService : ServiceBase
    {
        private static Timer _timer;
        private static LoggerDeArchivoDeTexto Logger { get; set; }
        public ProccesorService()
        {
            var ruta = ConfigurationManager.AppSettings["Ruta"];
            var archivo = ConfigurationManager.AppSettings["NombreArchivoLog"];
            Logger = new LoggerDeArchivoDeTexto(ruta, archivo, new List<string> { "Alta Estatus Especiales" }, true);
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            _timer = new Timer(10000); // 10 Seconds
            _timer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            _timer.Enabled = true;
        }

        private static void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            try
            {
                var kernel = new StandardKernel();
                kernel.Load(new EstatusEspecialesProcesadorModule());
                var lectorExcel = kernel.Get<ILector>();
                var conexionCassandra = kernel.Get<UbiqoCassandraBof>();

                var procesador = new Procesador(lectorExcel, conexionCassandra);
                procesador.Ejecutar();
            } catch(Exception ex)
            {
                Logger.AgregaMensajeDeErrorAsincrono(ex.ToString(), new List<string> { "Errores" });
            }
        }

        protected override void OnStop()
        {
            _timer.Stop();
        }

    }
}
