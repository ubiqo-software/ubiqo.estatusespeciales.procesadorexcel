﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using Ninject;
using System.Reflection;
using Ubiqo.EstatusEspeciales.ProcesadorExcel.Interfaces;
using System.Collections.Generic;
using Ubiqo.Cassandra;
using Cassandra;
using Ubiqo.EstatusEspeciales.ProcesadorExcel.Models;
// using Ubiqo.Cassandra;

namespace Ubiqo.EstatusEspeciales.ProcesadorExcel.Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestProcesadorExcelDirectoEnLector()
        {
            const string filePath = @"C:\EstatusEspeciales\SinProcesar\pruebapancho_8a5624ba-8e10-46ca-a5c9-8ede050104be.xlsx";
            var stream = File.Open(filePath, FileMode.Open, FileAccess.Read);
            var lector = new Lector();
            lector.ObtenerInformacionPorCelda(stream, new Guid("736B8A06-4E2E-4DFA-9B7D-A8EEE60E561A"), "CargaEstatusEspecial");
        }
        [TestMethod]
        public void TestProcesadorExcel()
        {
            var procesador = new Procesador(new Lector(), new UbiqoCassandraBof()); 
            procesador.Ejecutar();
        }

        [TestMethod]
        public void TestProcesadoExcelConNiject()
        {
            var kernel = new StandardKernel();
            kernel.Load(new EstatusEspecialesProcesadorModule());
            var lectorExcel = kernel.Get<ILector>();
            var conexionCassandra = kernel.Get<UbiqoCassandraBof>();
            Guid idUsuario = new Guid("8a5624ba-8e10-46ca-a5c9-8ede050104be");
            List<EquipoEstatusEspecial> estatusEquipos = new List<EquipoEstatusEspecial> { new EquipoEstatusEspecial { IdEquipo = 13843, IdEstatusEspecial = 2, Comentarios = "Probando en Motor antes de update en server"  } };

            var procesador = new Procesador(lectorExcel, conexionCassandra);
            procesador.GuardarBOFCassandra(estatusEquipos, idUsuario);
        }

        [TestMethod]
        public void SelectTest()
        {
            var kernel = new StandardKernel();

            kernel.Load(new List<Ninject.Modules.NinjectModule>() {
            new UbiqoCassandraModule()
            });

            var conexionCassandraBle = kernel.Get<UbiqoCassandraBof>();

            var session = conexionCassandraBle.GetSession();

            const string querySelectPorIdEquipoUno =
                @"SELECT 
                    id_equipo, fecha_cambio, comentario, id_estatus_especial, id_usuario
                FROM 
                    bof_log_cambio_estatus_especial
                WHERE id_equipo = ?
                limit 1";

            var preparedSelect = session.Prepare(querySelectPorIdEquipoUno);
            var equipoEstatusEspecial = session.Execute(preparedSelect.Bind(13843));

            const string querySelectIdEqupoTodos =
                @"SELECT 
                    id_equipo, fecha_cambio, comentario, id_estatus_especial, id_usuario
                FROM 
                    bof_log_cambio_estatus_especial
                WHERE id_equipo = ?";

            var preparedSelectTodos = session.Prepare(querySelectIdEqupoTodos);
            var equiposEstatusEspecial = session.Execute(preparedSelectTodos.Bind(13843));

            EquipoEstatusEspecial resultadoEstatus = new EquipoEstatusEspecial();

            foreach (var row in equipoEstatusEspecial)
            {
                var jIdEquipo = row.GetValue<int>("id_equipo");
                resultadoEstatus.IdEquipo = jIdEquipo;

                var jFechaCambio = row.GetValue<DateTimeOffset>("fecha_cambio");
                resultadoEstatus.FechaReporte = jFechaCambio;

                var jComentarios = row.GetValue<string>("comentario");
                resultadoEstatus.Comentarios = jComentarios;

                var jIdEstatusEspecial = (byte)row.GetValue<sbyte>("id_estatus_especial");
                resultadoEstatus.IdEstatusEspecial = jIdEstatusEspecial;
            }

            Console.WriteLine("IDEQUIPO: " + resultadoEstatus.IdEquipo);
            Console.WriteLine("FECHAREPORTE: " + resultadoEstatus.FechaReporte);
            Console.WriteLine("COMENTARIO: " + resultadoEstatus.Comentarios);
            Console.WriteLine("IDESTATUSESPECIAL: " + resultadoEstatus.IdEstatusEspecial);
        }

        [TestMethod]
        public void SelectTestSOme()
        {
            var kernel = new StandardKernel();

            kernel.Load(new List<Ninject.Modules.NinjectModule>() {
            new UbiqoCassandraModule()
            });

            var conexionCassandraBle = kernel.Get<UbiqoCassandraProd>();

            var session = conexionCassandraBle.GetSession();

            const string querySelect =
                @"SELECT 
                    *
                FROM 
                    eventos_sos_off_plataforma";

            var preparedSelect = session.Prepare(querySelect);
            var result = session.Execute(preparedSelect.Bind());

            
        }

        [TestMethod]
        public void SelectTest2()
        {
            var cluster = Cluster.Builder()
                                  .AddContactPoints(new string[3] { "88.198.185.230", "88.198.185.235", "88.198.185.240" })
                                  .WithPort(9042)
                                  .WithLoadBalancingPolicy(new DCAwareRoundRobinPolicy("dc1"))
                                  .WithAuthProvider(new PlainTextAuthProvider("cassandra", "Ubiq0C4s5Andr419$"))
                                  .Build();

            var session = cluster.Connect("ubiqo_ble");

            var idDispositivo = 27233;
            sbyte idTipoDispositivo = 1;

            const string querySelectPorUsuario =
                @"SELECT 
                    id_usuario AS IdUsuario,
                    comentario AS Comentario,
                    id_dispositivo AS IdDipositivo, 
                    tipo_dispositivo AS IdTipoDipositivo,
                    fecha_sos_off AS Fecha
                FROM 
                    eventos_sos_off_plataforma 
                WHERE 
                    id_dispositivo = ?
                    AND tipo_dispositivo = ?";

            var preparedSelect = session.Prepare(querySelectPorUsuario);
            var listaSosOff = session.Execute(preparedSelect.Bind(idDispositivo, idTipoDispositivo));
        }
    }
}
