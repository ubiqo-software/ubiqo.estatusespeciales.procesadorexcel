﻿using Softelligence.Log;
using System;
using System.Collections.Generic;
using System.IO;
using Ubiqo.EstatusEspeciales.ProcesadorExcel.Models;

namespace Ubiqo.EstatusEspeciales.ProcesadorExcel.Interfaces
{
    public interface ILector
    {
        List<EquipoEstatusEspecial> EquiposConEstatusEspecial { get; set; }
        LoggerDeArchivoDeTexto Logger { get; set; }
        List<EquipoEstatusEspecial> ObtenerInformacionPorCelda(Stream excel, Guid idUsuario, string nombreArchivo);
    }
}
