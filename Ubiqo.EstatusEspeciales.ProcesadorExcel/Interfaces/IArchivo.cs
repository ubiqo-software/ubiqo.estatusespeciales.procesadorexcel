﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ubiqo.EstatusEspeciales.ProcesadorExcel.Interfaces
{
    interface IArchivo
    {
        string Nombre { get; set; }
        string NombreConFecha { get; set; }
        string Contenido { get; set; }
    }
}
