﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ubiqo.EstatusEspeciales.ProcesadorExcel.Interfaces
{
    interface IArchivoPath
    {
        string RutaProcesado { get; set; }
        string RutaOrigen { get; set; }
    }
}
