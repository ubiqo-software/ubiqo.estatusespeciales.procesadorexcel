﻿using System;

namespace Ubiqo.EstatusEspeciales.ProcesadorExcel.Models
{
    public class EquipoEstatusEspecial
    {
        public int IdEquipo { get; set; }
        public string Alias { get; set; }
        public int IdEmpresa { get; set; }
        public string EstatusEspecial { get; set; }
        public byte IdEstatusEspecial { get; set; }
        public string Comentarios { get; set; }
        public DateTimeOffset FechaReporte { get; set; }
    }
}
