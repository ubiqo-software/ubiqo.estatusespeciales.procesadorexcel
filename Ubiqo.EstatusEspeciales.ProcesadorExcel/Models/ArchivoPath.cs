﻿using Ubiqo.EstatusEspeciales.ProcesadorExcel.Interfaces;

namespace Ubiqo.EstatusEspeciales.ProcesadorExcel.Models
{
    public class ArchivoPath : IArchivoPath
    {
        public string RutaErrores { get => @"C:\EstatusEspeciales\Errores"; set { } }
        public string RutaProcesado { get => @"C:\EstatusEspeciales\Procesados"; set { } }
        public string RutaOrigen { get => @"C:\EstatusEspeciales\SinProcesar"; set { } }
    }
}
