﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ubiqo.EstatusEspeciales.ProcesadorExcel.Models
{
    public class EstatusEspecial
    {
        public byte Id { get; set; }
        public string Nombre { get; set; }
    }
}
