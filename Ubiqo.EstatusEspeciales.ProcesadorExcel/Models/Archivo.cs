﻿using System;
using System.IO;
using Ubiqo.EstatusEspeciales.ProcesadorExcel.Interfaces;

namespace Ubiqo.EstatusEspeciales.ProcesadorExcel.Models
{
    public class Archivo : IArchivo
    {
        public string Nombre { get; set; }
        public string NombreConFecha { get; set; }
        public string Contenido { get; set; }
        public Guid IdUsuario { get; set; }

        public Archivo(string archivo)
        {
            Nombre = Path.GetFileName(archivo);
            var nombreConExtension = Nombre.Split('.');
            if (nombreConExtension.Length > 1)
                Nombre = nombreConExtension[0];
            NombreConFecha = Nombre + "_" + DateTime.Now.ToString("yyyyMMddHHmmss");

            var componentesNombre = Nombre.Split('_');
            if (componentesNombre.Length == 1)
                throw new FormatException($"El archivo {Nombre} no contiene el idUsuario");
            IdUsuario = new Guid(componentesNombre[1]);
            Contenido = archivo;
        }
    }
}
