﻿using Dapper;
using Softelligence.ConexionManager;
using Softelligence.Log;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Ubiqo.Cassandra;
using Ubiqo.EstatusEspeciales.ProcesadorExcel.Interfaces;
using Ubiqo.EstatusEspeciales.ProcesadorExcel.Models;

namespace Ubiqo.EstatusEspeciales.ProcesadorExcel
{
    public class Procesador
    {
        public ILector LectorExcel { get; set; }
        public UbiqoCassandraBof ConexionCassandra { get; set; }

        public Procesador(ILector lector, UbiqoCassandraBof conexionCassandra) {
            LectorExcel = lector;
            ConexionCassandra = conexionCassandra;
        }

        public void Ejecutar()
        {
            var path = new ArchivoPath();
            var archivos = Directory.GetFiles(path.RutaOrigen);

            if (archivos.Length > 0)
                LectorExcel.Logger.AgregaMensajeAsincrono($"Inicia procesamiento de {archivos.Length} archivos");

            foreach (var a in archivos)
            {
                try
                {
                    var archivo = new Archivo(a);
                    LectorExcel.Logger.AgregaMensajeAsincrono($"Procesando archivo {archivo.Nombre}: {DateTime.Now}");
                    try
                    {
                        var stream = File.Open(archivo.Contenido, FileMode.Open, FileAccess.Read);
                        var equiposEstatusEspecial = LectorExcel.ObtenerInformacionPorCelda(stream, archivo.IdUsuario, archivo.NombreConFecha);
                        equiposEstatusEspecial = ObtenerIdsEstatusEspecial(equiposEstatusEspecial);

                        GuardarEnBD(equiposEstatusEspecial);
                        GuardarBOFCassandra(equiposEstatusEspecial, archivo.IdUsuario);

                        MoverArchivoAProcesados(archivo, path);
                    }
                    catch (Exception ex)
                    {
                        MoverArchivoConErrores(archivo.Contenido, archivo.NombreConFecha, path);
                        LectorExcel.Logger.AgregaMensajeDeErrorAsincrono($"Al procesar {archivo.Nombre}: {ex}", new List<string> { "Errores" });
                    }
                } catch (Exception ex)
                {
                    MoverArchivoConErrores(a, Path.GetFileName(a), path);
                    LectorExcel.Logger.AgregaMensajeDeErrorAsincrono(ex.ToString(), new List<string> { "Errores" });

                }

            }
            
        }
        public void GuardarEnBD(List<EquipoEstatusEspecial> equiposEstatusEspecial)
        {
            ConexionFactory.Crea(conexion => {
                var transaccion = conexion.BeginTransaction();
         
                try
                {
                    var equiposEstatusEspecialAModificar = equiposEstatusEspecial.Where(x => x.IdEstatusEspecial != 0);
                    foreach (var r in equiposEstatusEspecialAModificar)
                    {
                         
                        conexion.Execute(@"UPDATE Dat_Equipos SET id_estatus_especial_equipo = @IdEstatus WHERE id = @IdEquipo", 
                            new { IdEstatus = r.IdEstatusEspecial, IdEquipo = r.IdEquipo }, transaccion);
                    }

                    transaccion.Commit();
                    return null;
                }
                catch (Exception ex)
                {
                    transaccion.Rollback();
                    LectorExcel.Logger.AgregaMensajeDeErrorAsincrono(ex.ToString(), new List<string> { "ErroresBD" });
                    return null;
                }
            });
            
        }

        public void GuardarBOFCassandra(List<EquipoEstatusEspecial> equiposEstatusEspecial, Guid idUsuario)
        {
            var session = ConexionCassandra.GetSession();
            const string queryInsert =
                   @"INSERT INTO bof_log_cambio_estatus_especial (id_equipo, fecha_cambio, comentario, id_estatus_especial, id_usuario)
                  VALUES (?,?,?,?,?)";

            foreach (var e in equiposEstatusEspecial)
            {
                var fechaCambio = DateTimeOffset.Now;
                var preparedStatement = session.Prepare(queryInsert);
                session.Execute(preparedStatement.Bind(e.IdEquipo, fechaCambio, e.Comentarios, (sbyte)e.IdEstatusEspecial, idUsuario));
            }
        }

        public List<EquipoEstatusEspecial> ObtenerIdsEquiposPorAlias(List<EquipoEstatusEspecial> equiposEstatusEspecial)
        {
            var equiposEstatusEspecialConIdEquipo = new List<EquipoEstatusEspecial>();
            var equiposAlias = equiposEstatusEspecial.Where(x => x.Alias != "").Select(x => x.Alias).Distinct().ToList();
            ConexionFactory.Crea(conexion =>
            {
                equiposEstatusEspecialConIdEquipo = conexion.Query<EquipoEstatusEspecial>(
                    @"SELECT id AS IdEquipo,
			                alias as Alias
		                FROM Dat_Equipos WITH(NOLOCK)
		                WHERE id_estatus_equipo <> 6 AND id_empresa = @IdEmpresa and alias in @AliasEquipos",
                    new { IdEmpresa = 0, AliasEquipos = equiposAlias }).ToList();
                return null;
            }, sololectura: true);

            foreach (var e in equiposEstatusEspecial)
            {
                e.IdEquipo = equiposEstatusEspecialConIdEquipo.Where(x => x.Alias == e.Alias).Select(x => x.IdEquipo).SingleOrDefault();
            }

            return equiposEstatusEspecial;
        }

        public List<EquipoEstatusEspecial> ObtenerIdsEstatusEspecial(List<EquipoEstatusEspecial> equiposEstatusEspecial)
        {
            var equiposEstatusEspecialConIdEquipo = new List<EstatusEspecial>();
            ConexionFactory.Crea(conexion =>
            {
                equiposEstatusEspecialConIdEquipo = conexion.Query<EstatusEspecial>(
                    @"SELECT id AS Id,
			                estatus_especial as Nombre
		                FROM Cat_EstatusEspecialEquipos WITH(NOLOCK)").ToList();
                return null;
            }, sololectura: true);

            foreach (var e in equiposEstatusEspecial)
            {
                e.IdEstatusEspecial = equiposEstatusEspecialConIdEquipo.Where(x => x.Nombre.ToLower() == e.EstatusEspecial.ToLower()).Select(x => x.Id).SingleOrDefault();
            }

            return equiposEstatusEspecial;
        }

        public void MoverArchivoAProcesados(Archivo archivo, ArchivoPath path) {
            Directory.CreateDirectory(path.RutaProcesado);
            var destFile = Path.Combine(path.RutaProcesado, archivo.NombreConFecha);
            File.Move(archivo.Contenido, destFile);
        }

        public void MoverArchivoConErrores(string archivo, string nombre, ArchivoPath path)
        {
            Directory.CreateDirectory(path.RutaErrores);
            var destFile = Path.Combine(path.RutaErrores, nombre);
            File.Move(archivo, destFile);
        }
    }
}
