﻿using ExcelDataReader;
using Softelligence.Log;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using Ubiqo.EstatusEspeciales.ProcesadorExcel.Interfaces;
using Ubiqo.EstatusEspeciales.ProcesadorExcel.Models;

namespace Ubiqo.EstatusEspeciales.ProcesadorExcel
{
    public class Lector : ILector
    {
        public List<EquipoEstatusEspecial> EquiposConEstatusEspecial { get; set; }
        public LoggerDeArchivoDeTexto Logger { get; set; }

        public Lector()
        {
            var ruta = ConfigurationManager.AppSettings["Ruta"];
            var archivo = ConfigurationManager.AppSettings["NombreArchivoLog"];
            Logger = new LoggerDeArchivoDeTexto(ruta, archivo, new List<string> { "Alta Estatus Especiales" }, true);
        }

        public List<EquipoEstatusEspecial> ObtenerInformacionPorCelda(Stream excel, Guid idUsuario, string nombreArchivo)
        {
            EquiposConEstatusEspecial = new List<EquipoEstatusEspecial>();

            using (var excelReader = ExcelReaderFactory.CreateOpenXmlReader(excel))
            {
                var dataSet = excelReader.AsDataSet(new ExcelDataSetConfiguration { UseColumnDataType = false, ConfigureDataTable = (tableReader) => new ExcelDataTableConfiguration { UseHeaderRow = true, ReadHeaderRow = (rowReader) => { } } });
                var table = dataSet.Tables[0];
                var rows = table.Rows;
                var numColumnas = table.Columns.Count - 1;

                for (var r = 0; r <= rows.Count - 1; r++)
                {
                    var rowsItem = rows[r].ItemArray;

                    var idEquipo = rowsItem[0] != DBNull.Value ? rowsItem[0].ToString() : "";
                    if (idEquipo == "")
                         new FormatException($"El registro {r + 1} no contiene idEquipo");

                    var estatusEspecial = rowsItem[1] != DBNull.Value ? rowsItem[1].ToString() : "";
                    if (estatusEspecial == "")
                        new FormatException($"El registro {r + 1} no contiene estatus");


                    //var idEmpresa = rowsItem[2] != DBNull.Value ? rowsItem[2].ToString() : "";
                    //if (idEmpresa == "")
                    //    new FormatException($"El registro {r + 1} no contiene idEmpresa");

                    var comentarios = rowsItem[2] != DBNull.Value ? rowsItem[2].ToString() : "";


                    var estatusEspecialDelEquipo = new EquipoEstatusEspecial
                    {
                        IdEquipo = Int32.Parse(idEquipo),
                        EstatusEspecial = estatusEspecial,
                        //IdEmpresa = Int32.Parse(idEmpresa),
                        Comentarios = comentarios
                    };

                    EquiposConEstatusEspecial.Add(estatusEspecialDelEquipo);
                }
            }

            return EquiposConEstatusEspecial;
        }
    }
}
