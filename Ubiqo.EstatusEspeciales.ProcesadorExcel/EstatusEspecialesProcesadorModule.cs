﻿using Ninject.Modules;
using Ubiqo.Cassandra;
using Ubiqo.EstatusEspeciales.ProcesadorExcel.Interfaces;

namespace Ubiqo.EstatusEspeciales.ProcesadorExcel
{
    public class EstatusEspecialesProcesadorModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ILector>().To<Lector>().InSingletonScope();
            Bind<UbiqoCassandraBle>().ToSelf().InSingletonScope();
            Bind<UbiqoCassandraBof>().ToSelf().InSingletonScope();
        }
    }
}
