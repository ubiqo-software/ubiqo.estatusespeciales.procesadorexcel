﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ubiqo.EstatusEspeciales.ProcesadorExcel.Tables.Types
{
    public class TypeIdVarchar
    {
        public string str_key { get; set; }
    }
}
